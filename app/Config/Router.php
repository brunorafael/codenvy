<?php
/**
 * Created by PhpStorm.
 * User: r-1
 * Date: 05/09/2016
 * Time: 15:21
 */

namespace app\Config;
use src\Controller\DefaultController;
use src\Controller\ProductController;


class Router
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var DefaultController
     */
    private $defaultController;

    /**
     * Construct the object
     * @param EntityManager $em
     */
    public function __construct($em) {
        $this->em = $em;
        $this->defaultController = new DefaultController($this->em);
    }


    public function routerRequest(){
        
        if(isset($_GET['controller']) && isset($_GET['action'])){
            
            $path = _CTRLPATH_.ucfirst($_GET['controller'].'Controller.php');            
            $action = $_GET['action'].'Action';
            
            if(file_exists($path)){
                $myController = new ProductController($this->em);
                if(method_exists($myController,$action)){
                    $myController->$action();
                }
                else{
                    $this->defaultController->indexAction();
                    echo 'Bad Action';
                }
            }
            else{
                $this->defaultController->indexAction();
                echo 'Bad Controller';
            }
        }
        else{
            $this->defaultController->indexAction();
           

        }
    }



}