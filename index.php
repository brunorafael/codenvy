<?php
ini_set("display_errors",1);
session_start();

use app\Config\Router;

define('_CTRLPATH_', 'src/Controller/');
define('_VIEWPATH_', 'src/View/');
define('_DOCTRINEPATH_', __DIR__.'/');
define('_INDEXPATH_', __DIR__.'/public/');

require_once "vendor/autoload.php";
require_once (_DOCTRINEPATH_.'bootstrap.php');

$router = new Router($em);
$router->routerRequest();