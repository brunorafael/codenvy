<?php
/**
 * Created by PhpStorm.
 * User: r-1
 * Date: 05/09/2016
 * Time: 16:34
 */

namespace src\Controller;


class DefaultController extends Controller
{

    public function indexAction()
    {
        $this->render('layout.php');
    }
}