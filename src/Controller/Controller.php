<?php

namespace src\Controller;

use Doctrine\ORM\EntityManager;

class Controller{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * Default constructor
     * @param EntityManager $em
     */
    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * Display the requested page with the requested parameters
     * @param string $name
     * @param array/bool $params
     */
    public function render($name, $params = false) {
        $thePath = _VIEWPATH_ . $name;
        if (file_exists($thePath)) {
            if ($params) {
                foreach ($params as $key => $value) {
                    $$key = $value;
                }
            }
            include_once $thePath;
        } else {
            // Default action
            echo "Vue manquante.";
        }
    }

    /**
     * Returns the EntityManager
     * @return EntityManager
     */
    public function getEntityManager() {
        return $this->em;
    }

    /**
     * Returns the requested EntityRepository
     * @param string $repo
     * @return EntityRepository
     */
    public function getRepository($repo) {
        return $this->em->getRepository($repo);
    }

}