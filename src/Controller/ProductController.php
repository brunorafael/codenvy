<?php 
//ProductController.php

namespace src\Controller;

use src\Entity\Product;

class ProductController extends Controller {

    public function listAction()
    {
        $repo = $this->em->getRepository('src\Entity\Product');
 //       var_dump($repo);
        $query = $repo->createQueryBuilder('p')
            ->orderBy('p.id', 'ASC')
            ->getQuery();
 //       var_dump($query);
        $products = $query->getResult();
 //       var_dump($products);
        $this->render('product.php', array('products' => $products));
    }

    public function addAction()
    {
        if (!empty($_POST)) {
            $prod = new Product();
            $prod->setName(htmlentities($_POST['name']));
            $this->em->persist($prod);
            $this->em->flush();
            // redirection
            header('Location: list');
        }else {
            $this->render('addProduct.php');
        }
    }

    public function modifyAction()
    {
        if (!empty($_GET['id'])) {
            $repo = $this->em->getRepository('src\Entity\Product');
            $idToModify = (int)$_GET['id'];
            $productToModify = $repo->findOneById($idToModify);

            if (!empty($_POST)) {
                $productToModify->setName(htmlentities($_POST['name']));
                $this->em->flush();
                header('Location: ../list');
            }
            else {
                $this->render('addProduct.php', array(
                    'productToModify' => $productToModify
                ));
            }
        }
        else{
            header('Location: ../list');
        }
    }

    public function removeAction() {
        $repo = $this->em->getRepository('src\Entity\Product');
        $idToModify = (int)$_GET['id'];
        $productToModify = $repo->findOneById($idToModify);
        $this->em->remove($productToModify);
        $this->em->flush();
        header('Location: ../list');
    }

}