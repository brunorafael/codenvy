<!doctype html>
<html lang="fr">
<head>
    <?php include_once "header.php"; ?>
    <title>Produits</title>
</head>
<body>
<div class="container">
    <?php include_once "menu.php"; ?>
    <h1>Liste des produits</h1>
    <p><a href="add">Ajouter un produit</a></p>
    <hr/>

    <table class="table table-striped">
        <thead>
        <th>ID</th>
        <th>Nom</th>
        <th></th>
        <th></th>
        </thead>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td><?= $product->getID() ?></td>
                <td><?= $product->getName() ?></td>
                <td><a href="modify/<?= $product->getId() ?>">Modifier</a></td>
                <td><a onClick="return confirm('Voulez-vous vraiment supprimer ce objet?');" href="remove/<?= $product->getId() ?>">Supprimer</a></td>
            </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>