<!doctype html>
<html lang="fr">
<head>
    <?php include_once "header.php"; ?>
    <title>Add product</title>
</head>
<body>
<div class="container">
    <?php include_once "menu.php"; ?>
    <h1>Produits</h1>

    <?php if (isset($productToModify)) { ?>    
    <form method="post"
          action="<?= $productToModify->getId(); ?>"
          class="form-group">
        <?php } else { ?>
        <form method="post"
              action="add"
              class="form-group">
            <?php } ?>
            <fieldset>
                <?php if (isset($productToModify)) { ?>
                    <legend>Modifier un Produit</legend>
                <?php } else { ?>
                    <legend>Ajouter un Produit</legend>
                <?php } ?>
                <div>
                    <div class="from-group">
                        <label for="name">Nom :</label>
                        <input
                            type="text"
                            name="name"
                            value="<?= isset($productToModify) ? $productToModify->getName() : ''; ?>"
                            id="name"
                            class="form-control"
                            placeholder="Nom du produit">
                    </div>

                </div>
                <div class="from-group">
                    <?php if (isset($productToModify)) { ?>
                        <button type="submit" class="btn btn-default">Modifier</button>
                    <?php } else { ?>
                        <button type="submit" class="btn btn-default">Ajouter</button>
                    <?php } ?>
                </div>
            </fieldset>

        </form>
</div>
</body>
</html>