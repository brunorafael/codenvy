<?php //Product.php

namespace src\Entity;


/**
* @Table(name="products")
* @Entity(repositoryClass="src\Repository\ProductRepository")
**/
class Product{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
	protected $id;
    /**
     * @Column(type="string", name="product_name", length=50)
     */
	protected $name;

	public function getId(){
		return $this->id;
	}
	public function getName(){
		return $this->name;
	}
	public function setName($name){
		$this->name = $name;
	}
}