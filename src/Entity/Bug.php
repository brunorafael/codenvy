<?php
// src/Bug.php

namespace src\Entity;

/**
 * @Entity @Table(name="bugs")
 **/
use Doctrine\Common\Collections\ArrayCollection;

class Bug
{
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    private $id;
    /**
     * @Column(type="string")
     **/
    private $description;
    /**
     * @Column(type="datetime")
     **/
    private $created;
    /**
     * @Column(type="string")
     **/
    private $status;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="assignedBugs")
     **/
    private $engineer;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="reportedBugs")
     **/
    private $reporter;

    /**
     * @ManyToMany(targetEntity="Product")
     **/
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();

    }



}//end class.