<?php // bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// Load composer autoload
require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
 $isDevMode = true; 
 $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/Entity"), $isDevMode); 

// Database configuration parameters
 $conn = array( 'driver' => 'pdo_mysql',
                'user'  =>  'root',
                'password' => 'brsilva',
        		'port' => 3306,
        		'host' => '127.0.0.1',
        		'dbname' => 'doctrine_dl16'
        	);
// Obtaining the entity manager from a factory
 $em = EntityManager::create($conn, $config);