var gulp = require('gulp')
var sass = require('gulp-sass')

//taks for sass
gulp.task('sass', function(){
    gulp.src('public/sass/**/*.sass')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('public/css'))
    
})

//taks for watch
gulp.task('watch', () =>
    gulp.watch('public/sass/**/*.sass', ['sass'])
)

//taks default
gulp.task('default',['watch'])